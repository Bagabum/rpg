﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistantCamera : MonoBehaviour {

	[SerializeField]
	private float sensitivity = 0.5f;

	private float RotationX = 0;
	private float RotationY = 0;
	private Transform mainCamera;

	// Use this for initialization
	void Start () {
		mainCamera = Camera.main.transform;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButton (2)) {
			RotationX = Input.GetAxis("Mouse X") * sensitivity;
			RotationY = Input.GetAxis("Mouse Y") * -sensitivity;
			mainCamera.RotateAround (transform.position, Vector3.up, RotationX);
			mainCamera.RotateAround (transform.position, Vector3.right, RotationY);
			mainCamera.rotation = Quaternion.LookRotation (transform.position - mainCamera.position);
			mainCamera.eulerAngles = new Vector3(mainCamera.eulerAngles.x, mainCamera.eulerAngles.y, 0);
		}
	}
}
