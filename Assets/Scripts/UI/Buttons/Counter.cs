﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Counter : MonoBehaviour
{

    [SerializeField]
    private Text text;
    private int Count;

    void Start()
    {
        Count = 0;
    }

    public void AddCount()
    {
        Count++;
        text.text = "Вы нажали " + Count + " раз";
    }

    public void StartScene()
    {
        SceneManager.LoadScene("Lesson2");
    }

    public void Pause()
    {
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
        else
        {
            Time.timeScale = 0;
        }
    }

}
