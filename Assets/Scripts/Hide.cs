﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hide : MonoBehaviour {

	[SerializeField] private Renderer cube;

	void Start () {

	}

	void Update () {
		if (Input.GetKeyDown(KeyCode.Space)) {
			StartCoroutine("Fade");
		}
	}

	IEnumerator Fade() {
		for (float i = 1; i > 0; i -= 0.1f)
		{
			Color color = cube.material.color;
			color.a = i;
			cube.material.color = color;
			yield return new WaitForSeconds(1f); // null
		}
	}
	
}
