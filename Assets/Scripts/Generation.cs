﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generation : MonoBehaviour {

    [SerializeField]
    private GameObject wall;
    [SerializeField]
    private Transform ground;
    [SerializeField]
    private int count = 5;
    private int shift = 15;

    void Start () {
		if (wall != null && ground != null)
        {
            for (int i = 0; i < count; i++)
            {
                for (int j = 0; j < count; j++)
                {
                    GameObject gameWall = Instantiate(wall, new Vector3(i * 2 - count / 2, 1.5f, j * 2 + shift), Quaternion.identity);
                    Collider box = gameWall.GetComponent<Collider>();
                    box.isTrigger = true;
                    MeshRenderer mesh = gameWall.GetComponent<MeshRenderer>();
                    mesh.material.color = new Color(Random.value, Random.value, Random.value);
                }
            }
        }
	}

}
