﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveTo : MonoBehaviour {

	NavMeshAgent agent;
	[SerializeField] private Animator animator;

	void Start () {
		agent = GetComponent<NavMeshAgent> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButton(0)) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit, Mathf.Infinity)) {
				agent.SetDestination (hit.point);
			}
		}

		SetWalk();
	}

	void SetWalk() {
		if (animator != null) {

			animator.SetFloat("Blend", agent.velocity.magnitude / agent.speed, 0.1f, Time.deltaTime);
		}
	}

}
