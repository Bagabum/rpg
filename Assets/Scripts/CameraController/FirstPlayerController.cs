﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPlayerController : MonoBehaviour {

    [SerializeField]
    private float sensitivity = 0.5f;
    [SerializeField]
    private float maxX = 90;
    [SerializeField]
    private float minX = -90;
    [SerializeField]
    private float smoth = 5;
    private float RotationX = 0;
    private float RotationY = 0;
    private Camera mainCamera;

    private void Start()
    {
        mainCamera = Camera.main;
        mainCamera.transform.SetParent(transform);
        mainCamera.transform.localPosition = new Vector3(0, 1, 0);
    }

    // Update is called once per frame
    void Update () {
        RotationX -= Input.GetAxis("Mouse Y") * sensitivity;
        RotationY += Input.GetAxis("Mouse X") * sensitivity;

        RotationX = Mathf.Clamp(RotationX, minX, maxX);

        mainCamera.transform.localRotation = Quaternion.Slerp(mainCamera.transform.localRotation, Quaternion.Euler(RotationX, 0, 0), smoth * Time.deltaTime);
        transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(0, RotationY, 0), smoth * Time.deltaTime);
    }
}
