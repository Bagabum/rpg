﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCollor : MonoBehaviour {

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Wall")
        {
            Change(collision.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Wall")
        {
            Change(other.gameObject);
        }
    }

    public void Change(GameObject other)
    {
        MeshRenderer mesh = GetComponent<MeshRenderer>();
        MeshRenderer collisionMesh = other.GetComponent<MeshRenderer>();
        mesh.material.color = collisionMesh.material.color;
    }

}
