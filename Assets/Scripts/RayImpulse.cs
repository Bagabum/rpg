﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayImpulse : MonoBehaviour
{
    [SerializeField]
    private float maxDistance = 10f;
    private Camera mainCamera;
    [SerializeField]
    private float impulse = 5;

    private void Start()
    {
        mainCamera = Camera.main;
    }

    void Update()
    {
        Debug.DrawRay(mainCamera.transform.position, mainCamera.transform.forward * maxDistance);
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = new Ray(mainCamera.transform.position, mainCamera.transform.forward);
            RaycastHit hit;
            Physics.Raycast(ray, out hit, maxDistance);
            if (hit.collider != null && hit.collider.tag == "Wall")
            {
                hit.rigidbody.AddForce(hit.point.normalized * impulse, ForceMode.Impulse);
            }
        }
    }
}
