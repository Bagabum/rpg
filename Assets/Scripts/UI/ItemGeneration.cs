﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemGeneration : MonoBehaviour {

	[SerializeField] private Canvas canvas;
	private Transform itemObject;	
	private GameObject textObject;	

	void Start () {
		if (canvas != null) {
			itemObject = GameObject.FindGameObjectWithTag("Item").transform;
			textObject = new GameObject();
			textObject.transform.SetParent(canvas.transform);
			Text text = textObject.AddComponent<Text>();
			text.text = "Poop";
			Font arial = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");
			text.font = arial;
			text.fontSize = 1;
			text.alignment = TextAnchor.MiddleCenter;
			text.material = arial.material;
			textObject.transform.position = new Vector3(itemObject.position.x, itemObject.position.y + 2, itemObject.position.z);
			textObject.SetActive(false);
			Item myItem = itemObject.GetComponent<Item>();
			myItem.text = textObject;
		}
	}
	
	void Update () {
		if (itemObject != null) {
			textObject.transform.position = new Vector3(itemObject.position.x, itemObject.position.y + 2, itemObject.position.z);
		}
	}
}
